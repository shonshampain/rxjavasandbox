import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

class RxBus {

    public enum Commands { HELLO_WORLD }
    public interface Action { void onCommand(); }

    private PublishSubject<Object> bus;

    RxBus() {
        bus = PublishSubject.create();
    }

    void post(Commands c) {
        bus.onNext(c);
    }

    Disposable consume(Commands command, Action action) {
        Disposable d = bus.subscribe(item -> {
            if (item == command) {
                action.onCommand();
            }
        });
        return d;
    }
}
