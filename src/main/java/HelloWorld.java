import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class HelloWorld {

    private Observable<String> hw;

    private HelloWorld() {
        String text = "Hello RxJava!";
        hw = Observable.just(text);
    }

    private Observable<String> getHelloWorldObservable() {
        return hw;
    }

    private void run() {
        Observable<String> o = getHelloWorldObservable();
        Disposable d = o.subscribe(System.out::println);
        d.dispose();
    }

    public static void main(String[] args) {
        HelloWorld hello = new HelloWorld();
        hello.run();
    }
}
