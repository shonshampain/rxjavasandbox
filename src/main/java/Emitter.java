import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;

public class Emitter {

    private Observable<String> emitter;

    private Emitter() {
        emitter = Observable.create(this::emit);
    }

    private void emit(ObservableEmitter<String> emitter) {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("foo");
        strings.add("bar");
        strings.add("in");
        strings.add("the");
        strings.add("morning");
        strings.add("is");
        strings.add("good");
        strings.add("for");
        strings.add("you");
        for (String s : strings) {
            emitter.onNext(s);
        }
        emitter.onComplete();
    }

    private Observable<String> getEmitterObservable() {
        return emitter;
    }

    private void run() {
        Observable<String> o = getEmitterObservable();
        Disposable d = o.subscribe(System.out::println);
        d.dispose();
    }

    public static void main(String[] args) {
        Emitter e = new Emitter();
        e.run();
    }
}
