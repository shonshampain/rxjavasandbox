import io.reactivex.disposables.Disposable;

public class RxBusHelloWorld {

    RxBus rxBus;
    Disposable d;

    private RxBusHelloWorld() {
        rxBus = new RxBus();
        d = rxBus.consume(RxBus.Commands.HELLO_WORLD, () -> System.out.println("Hello RxBus!"));
    }

    private void run() {
        rxBus.post(RxBus.Commands.HELLO_WORLD);
    }

    private void destroy() {
        d.dispose();
    }

    public static void main(String[] args) {
        RxBusHelloWorld r = new RxBusHelloWorld();
        r.run();
        r.destroy();
    }
}
